# Golang Lorca Vue Frontend
This project is vuejs frontend for [Lorca-vue-backend](https://framagit.org/hiva-iot/lorca-vue-backend) project.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
